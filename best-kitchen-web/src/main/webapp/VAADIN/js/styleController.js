/**
 * 
 */
function setPropertyRule(ruleName, property, valProperty) {
	for (var s = 0; s < document.styleSheets.length; s++) {

		var styleSheet = document.styleSheets[s];

		// examina si la hoja de estilos tiene reglas atraves del DOOM
		var rules = styleSheet.cssRules ? styleSheet.cssRules
				: styleSheet.rules;

		if (rules == null) {
			return null;
		}

		for (var i = 0; i < rules.length; i++) {
			if (rules[i].selectorText == ruleName) {
				rules[i].style[property] = valProperty;
			}
		}

	}
	return null;
}

function setURL(newURL) {
	try {
		window.history.pushState(newURL, newURL, newURL);
	} catch (e) {
		alert(e.message);
	}
}