package com.conexia.view;

import com.conexia.business.ConexiaBusiness;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Theme("valo")
@SuppressWarnings("serial")
public class TableView extends VerticalLayout implements View{

	public TableView() {
		final VerticalLayout layout = new VerticalLayout();
		layout.addComponent(new Label("Register Table"));

		final Label lblMaxDinner = new Label("Max Dinner");
		final TextField txtMaxDinner = new TextField();
		
		final Label lblLocation = new Label("Location");
		final TextField txtLocation = new TextField();
		
        Button saveBttn = new Button("Save");
        saveBttn.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
            	try{
            		ConexiaBusiness.getInstance().createOrUpdateTable(null, Integer.parseInt(txtMaxDinner.getValue()), Integer.parseInt(txtLocation.getValue()));
            	} catch (Exception e) {
            		Notification.show(e.getMessage());
				}
            	
            	Notification.show("Saved!");
            }
        });
		
        Button backBttn = new Button("Back to Bill menu");
        backBttn.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
            	UI.getCurrent().getNavigator().navigateTo("bill_view");
            }
        });

        layout.addComponent(lblMaxDinner);
        layout.addComponent(txtMaxDinner);
        layout.addComponent(lblLocation);
        layout.addComponent(txtLocation);
        layout.addComponent(saveBttn);
        layout.addComponent(backBttn);
        
        addComponent(layout);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
	}

}
