package com.conexia.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the service database table.
 * 
 */
@Entity
@NamedQueries({
})
public class Bill implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BILL_ID_GENERATOR", sequenceName="BILL_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BILL_ID_GENERATOR")
	private Long id;

	@ManyToOne
	@JoinColumn(name="customer_fk")
	private Customer customer;
	
	@ManyToOne
	@JoinColumn(name="table_fk")
	private Table table;
	
	@ManyToOne
	@JoinColumn(name="waiter_fk")
	private Waiter waiter;
	
	@Column(name="bill_timestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date billTimestamp;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Table getTable() {
		return table;
	}

	public void setTable(Table table) {
		this.table = table;
	}

	public Waiter getWaiter() {
		return waiter;
	}

	public void setWaiter(Waiter waiter) {
		this.waiter = waiter;
	}

	public Date getBillTimestamp() {
		return billTimestamp;
	}

	public void setBillTimestamp(Date billTimestamp) {
		this.billTimestamp = billTimestamp;
	}

	@Override
	public String toString() {
		return "Bill [id=" + id + ", customer=" + customer + ", table=" + table + ", waiter=" + waiter + ", billTimestamp="
				+ billTimestamp + "]";
	}
}