package com.conexia.view;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Theme("valo")
@SuppressWarnings("serial")
public class BillView extends VerticalLayout implements View{

	public BillView() {
		final VerticalLayout layout = new VerticalLayout();
		layout.addComponent(new Label("Bill Menu"));

        Button billInfoBtn = new Button("Create Bill");
        billInfoBtn.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
            	UI.getCurrent().getNavigator().navigateTo("bill_info_view");
            }
        });
		
        Button waiterBtn = new Button("Register Waiter");
        waiterBtn.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
            	UI.getCurrent().getNavigator().navigateTo("waiter_view");
            }
        });
        
        Button customerBtn = new Button("Register Customer");
        customerBtn.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
            	UI.getCurrent().getNavigator().navigateTo("customer_view");
            }
        });
        
        Button chefBtn = new Button("Register Chef");
        chefBtn.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
            	UI.getCurrent().getNavigator().navigateTo("chef_view");
            }
        });
        
        Button tableBtn = new Button("Register Table");
        tableBtn.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
            	UI.getCurrent().getNavigator().navigateTo("table_view");
            }
        });
        
        Button backBttn = new Button("Back to main menu ");
        backBttn.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
            	UI.getCurrent().getNavigator().navigateTo("");
            }
        });

        layout.addComponent(billInfoBtn);
        layout.addComponent(waiterBtn);
        layout.addComponent(customerBtn);
        layout.addComponent(chefBtn);
        layout.addComponent(tableBtn);
        layout.addComponent(backBttn);
        
        addComponent(layout);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
	}

}
