# Conexia's Best Kitchen

Welcome to Conexia's Best Kitchen. This is an application composed of an ear module, an ejb module, a commons module in a jar, a web module

## Getting Started

All you need is Java 8, Maven and PostgreSQL

### Prerequisites

You'll find the file best_kitchen.sql file, cointaining the database structure for the study case.

## Running Conexia's Best Kitchen

It is required to configure a datasource with the name BestKitchen on your applications server, as described on the file datasource.txt 

To run, just install the project when located on /best-kitchen project.

It is also required to have the postgresql-42.2.5.jar up to date and able to deploy on your applications server.

```
mvn install
```

## Using Conexia's Best Kitchen

After deploying and running the project, you can navigate on the app using 
http://localhost:8080/best-kitchen/

Remember, the port can change if your application server is configured on another port for web applications.
