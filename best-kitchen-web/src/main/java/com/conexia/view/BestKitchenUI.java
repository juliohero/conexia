package com.conexia.view;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;

@Theme("valo")
@SuppressWarnings("serial")
public class BestKitchenUI extends UI {

	Navigator navigator;
	
    @WebServlet(value = "/*", asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = BestKitchenUI.class)
    public static class Servlet extends VaadinServlet {
    }

    @Override
    protected void init(VaadinRequest request) {
    	
    	navigator = new Navigator(this, this);
    	navigator.addView("", new IndexView());
    	navigator.addView("bill_view", new BillView());
    	navigator.addView("customer_view", new CustomerView());
    	navigator.addView("chef_view", new ChefView());
    	navigator.addView("table_view", new TableView());
    	navigator.addView("waiter_view", new WaiterView());
    	navigator.addView("bill_info_view", new BillInfoView());
    	navigator.addView("waiters_info_view" , new BestCustomersView());
    	navigator.addView("best_customers_view" , new WaitersInfoView());

    }

}
