package com.conexia.business;

import java.util.Date;
import java.util.List;

import com.conexia.dto.BestCustomers;
import com.conexia.dto.CustomerVO;
import com.conexia.dto.PersonVO;
import com.conexia.dto.TableVO;
import com.conexia.dto.WaitersInfo;
import com.conexia.exception.BestKitchenException;
import com.conexia.inteface.ConexiaServiceRemote;
import com.conexia.util.ServiceLocator;

public class ConexiaBusiness {
	
	private static ConexiaBusiness conexiaBusiness;
	
	private ConexiaServiceRemote conexiaRemoteService;
	
	public ConexiaBusiness() {
		conexiaRemoteService = ServiceLocator.getInstance().locateRemoteEJBStateless(ConexiaServiceRemote.class, "best-kitchen", "best-kitchen-ejb",
				"ConexiaService");
	}

	public static ConexiaBusiness getInstance() {
		if (conexiaBusiness == null) {
			conexiaBusiness = new ConexiaBusiness();
		}
		return conexiaBusiness;
	}
	
	public void createOrUpdateCustomer(Long id, String firstName, String lastName1, String lastName2, String observation)throws BestKitchenException {
		conexiaRemoteService.createOrUpdateCustomer(id, firstName, lastName1, lastName2, observation);
	}

	public void createOrUpdateChef(Long id, String firstName, String lastName1, String lastName2) throws BestKitchenException {
		conexiaRemoteService.createOrUpdateChef(id, firstName, lastName1, lastName2);
	}
	
	public void createOrUpdateTable(Long id, Integer maxDinnerSize, Integer location) throws BestKitchenException {
		conexiaRemoteService.createOrUpdateTable(id, maxDinnerSize, location);
	}
	
	public void createOrUpdateWaiter(Long id, String firstName, String lastName1, String lastName2) throws BestKitchenException {
		conexiaRemoteService.createOrUpdateWaiter(id, firstName, lastName1, lastName2);
	}
	
	public Long createBill(Long customerId, Long tableId, Long waiterId, Date billTimestamp) throws BestKitchenException {
		return conexiaRemoteService.createBill(customerId, tableId, waiterId, billTimestamp);
	}
	
	public Long saveDetail(Long billId, Long chefId, Double value, String dinnerPlate) throws BestKitchenException {
		return conexiaRemoteService.saveDetail(billId, chefId, value, dinnerPlate);
	}
	
	public List<CustomerVO> findCustomers() throws BestKitchenException {
		return conexiaRemoteService.findCustomers();
	}
	
	public List<PersonVO> findChefs() throws BestKitchenException {
		return conexiaRemoteService.findChefs();
	}
	
	public List<TableVO> findTables() throws BestKitchenException {
		return conexiaRemoteService.findTables();
	}
	
	public List<PersonVO> findWaiters() throws BestKitchenException {
		return conexiaRemoteService.findWaiters();
	}
	
	public List<WaitersInfo> findWaitersTotalByMonth() throws BestKitchenException {
		return conexiaRemoteService.findWaitersTotalByMonth();
	}
	
	public List<BestCustomers> findBestCustomers(Long maxValue) throws BestKitchenException {
		return conexiaRemoteService.findBestCustomers(maxValue);
	}
}
