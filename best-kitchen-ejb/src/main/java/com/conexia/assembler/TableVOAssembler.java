package com.conexia.assembler;

import java.util.List;

import com.conexia.dto.TableVO;
import com.conexia.dto.VOAssembler;
import com.conexia.dto.VOAssemblerException;
import com.conexia.entity.Table;

public class TableVOAssembler extends VOAssembler<Table, TableVO> {

	private static final TableVOAssembler instance = new TableVOAssembler();
	 
    private TableVOAssembler() {}
 
    public static TableVOAssembler getInstance() {
        return instance;
    }

    @Override
    public void assembleVOToEntity(TableVO vo, Table entity) throws VOAssemblerException{
		super.assembleVOToEntity(vo, entity);
		
	}
	
    @Override
	public void assembleVOListToEntityList(List<TableVO> voList, List<Table> entityList) throws VOAssemblerException{
		if(voList.size() != entityList.size()) throw new VOAssemblerException("Different size list");
		for(int i=0; i<voList.size(); i++){
			this.assembleVOToEntity(voList.get(i), entityList.get(i));
		}
	}
	
    @Override
	public void assembleEntityToVO(Table entity, TableVO vo) throws VOAssemblerException{
		super.assembleEntityToVO(entity, vo);
	}
	
    @Override
	public void assembleEntityListToVOList(List<Table> entityList, List<TableVO> voList) throws VOAssemblerException{
		if(voList.size() != entityList.size()) throw new VOAssemblerException("Different size list");
		for(int i=0; i<entityList.size(); i++){
			this.assembleEntityToVO(entityList.get(i), voList.get(i));
		}
	}
    
}
