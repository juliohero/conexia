package com.conexia.view;

import com.conexia.business.ConexiaBusiness;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Theme("valo")
@SuppressWarnings("serial")
public class CustomerView extends VerticalLayout implements View{

	public CustomerView() {
		final VerticalLayout layout = new VerticalLayout();
		layout.addComponent(new Label("Register customer"));

		final Label lblName = new Label("Name");
		final TextField txtName = new TextField();
		
		final Label lblLastName1 = new Label("Last Name 1");
		final TextField txtLastName1 = new TextField();
		
		final Label lblLastName2 = new Label("Last Name 2");
		final TextField txtLastName2 = new TextField();
		
		final Label lblObservation = new Label("Observation");
		final TextField obeservation = new TextField();
		
        Button saveBttn = new Button("Save");
        saveBttn.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
            	try{
            		ConexiaBusiness.getInstance().createOrUpdateCustomer(null, txtName.getValue(), txtLastName1.getValue(), txtLastName2.getValue(), obeservation.getValue());
            	} catch (Exception e) {
            		Notification.show(e.getMessage());
				}
            	
            	Notification.show("Saved!");
            }
        });
		
        Button backBttn = new Button("Back to Bill menu");
        backBttn.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
            	UI.getCurrent().getNavigator().navigateTo("bill_view");
            }
        });

        layout.addComponent(lblName);
        layout.addComponent(txtName);
        layout.addComponent(lblLastName1);
        layout.addComponent(txtLastName1);
        layout.addComponent(lblLastName2);
        layout.addComponent(txtLastName2);
        layout.addComponent(lblObservation);
        layout.addComponent(obeservation);
        layout.addComponent(saveBttn);
        layout.addComponent(backBttn);
        
        addComponent(layout);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
	}

}
