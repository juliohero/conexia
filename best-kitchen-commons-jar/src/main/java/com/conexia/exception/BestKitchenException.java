package com.conexia.exception;

public class BestKitchenException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public BestKitchenException (String message){
		super(message);
	}
	
	public BestKitchenException (Throwable cause){
		super(cause);
	}
	
	public BestKitchenException (String message, Throwable cause){
		super(message, cause);
	}
}
