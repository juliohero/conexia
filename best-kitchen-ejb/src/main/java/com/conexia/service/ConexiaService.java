package com.conexia.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import com.conexia.assembler.ChefVOAssembler;
import com.conexia.assembler.CustomerVOAssembler;
import com.conexia.assembler.TableVOAssembler;
import com.conexia.assembler.WaiterVOAssembler;
import com.conexia.dto.BestCustomers;
import com.conexia.dto.CustomerVO;
import com.conexia.dto.PersonVO;
import com.conexia.dto.TableVO;
import com.conexia.dto.WaitersInfo;
import com.conexia.entity.Bill;
import com.conexia.entity.BillDetail;
import com.conexia.entity.Chef;
import com.conexia.entity.Customer;
import com.conexia.entity.Table;
import com.conexia.entity.Waiter;
import com.conexia.exception.BestKitchenException;
import com.conexia.inteface.ConexiaServiceRemote;
import com.conexia.util.ListUtil;

@Stateless
public class ConexiaService implements ConexiaServiceRemote{

	@PersistenceContext
	private EntityManager em;
	
	@Resource(lookup = "java:jboss/datasources/BestKitchen")
	private DataSource dataSource;
	
	public List<CustomerVO> findCustomers() throws BestKitchenException{
		try {
			List<Customer> customers = (List<Customer>) em.createNamedQuery("Customer.findAll").getResultList();
			List<CustomerVO> customersVoList = ListUtil.createDefaultInitializedList(CustomerVO.class, customers.size());
			CustomerVOAssembler.getInstance().assembleEntityListToVOList(customers, customersVoList);
			return customersVoList;
		} catch (Exception e) {
			throw new BestKitchenException("Error while getting customers " + e.getMessage());
		}
	}
	
	public List<TableVO> findTables() throws BestKitchenException{
		
		StringBuffer waitersSql = new StringBuffer(
				"SELECT t.id, t.max_diner_size, t.location FROM table_bill t");
		
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		List<TableVO> bestCumstomersList = new ArrayList<TableVO>(); 
		
		try {
			conn = dataSource.getConnection();
			pst = conn.prepareStatement(waitersSql.toString());
			pst.executeQuery();
			rs = pst.getResultSet();

			while (rs.next()) {
				TableVO t = new TableVO();
				t.setId(rs.getLong(1));
				t.setMaxDinner(rs.getInt(2));
				t.setLocation(rs.getInt(3));
				
				bestCumstomersList.add(t);
			}
			
		} catch (SQLException e) {
			throw new BestKitchenException("Error while finding tables");
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
				if (conn != null)
					conn.close();
			} catch (SQLException ex) {
				throw new BestKitchenException("Error while finding waiters info");
			}
		}
		
		return bestCumstomersList;
	}
	
	public List<PersonVO> findWaiters() throws BestKitchenException{
		try {
			List<Waiter> waiters = (List<Waiter>) em.createNamedQuery("Waiter.findAll").getResultList();
			List<PersonVO> personVoList = ListUtil.createDefaultInitializedList(PersonVO.class, waiters.size());
			WaiterVOAssembler.getInstance().assembleEntityListToVOList(waiters, personVoList);
			return personVoList;
		} catch (Exception e) {
			throw new BestKitchenException("Error while getting waiters " + e.getMessage());
		}
	}
	
	public List<PersonVO> findChefs() throws BestKitchenException{
		try {
			List<Chef> chefs = (List<Chef>) em.createNamedQuery("Chef.findAll").getResultList();
			List<PersonVO> chefVoList = ListUtil.createDefaultInitializedList(PersonVO.class, chefs.size());
			ChefVOAssembler.getInstance().assembleEntityListToVOList(chefs, chefVoList);
			return chefVoList;
		} catch (Exception e) {
			throw new BestKitchenException("Error while getting chefs " + e.getMessage());
		}
	}
	
	public void createOrUpdateCustomer(Long id, String firstName, String lastName1, String lastName2, String observation) throws BestKitchenException{
		
		try {
			
			if (id != null) {
				Customer customer = em.find(Customer.class, id);
				customer.setName(firstName);
				customer.setLastName1(lastName1);
				customer.setLastName2(lastName2);
				customer.setObservation(observation);
				em.merge(customer);
			} else {
				Customer customer = new Customer();
				customer.setName(firstName);
				customer.setLastName1(lastName1);
				customer.setLastName2(lastName2);
				customer.setObservation(observation);
				em.persist(customer);
			}
		} catch (Exception e) {
			throw new BestKitchenException("Error while creating customer " + e.getMessage());
		}
	}
	
	public void createOrUpdateChef(Long id, String firstName, String lastName1, String lastName2) throws BestKitchenException{
		
		try {
			
			if (id != null) {
				Chef chef = em.find(Chef.class, id);
				chef.setName(firstName);
				chef.setLastName1(lastName1);
				chef.setLastName2(lastName2);
				em.merge(chef);
			} else {
				Chef chef = new Chef();
				chef.setName(firstName);
				chef.setLastName1(lastName1);
				chef.setLastName2(lastName2);
				em.persist(chef);
			}
		} catch (Exception e) {
			throw new BestKitchenException("Error while creating chef " + e.getMessage());
		}
	}
	
	public void createOrUpdateWaiter(Long id, String firstName, String lastName1, String lastName2) throws BestKitchenException{
		
		try {
			
			if (id != null) {
				Waiter waiter = em.find(Waiter.class, id);
				waiter.setName(firstName);
				waiter.setLastName1(lastName1);
				waiter.setLastName2(lastName2);
				em.merge(waiter);
			} else {
				Waiter waiter = new Waiter();
				waiter.setName(firstName);
				waiter.setLastName1(lastName1);
				waiter.setLastName2(lastName2);
				em.persist(waiter);
			}
		} catch (Exception e) {
			throw new BestKitchenException("Error while creating waiter " + e.getMessage());
		}
	}
	
	public void createOrUpdateTable(Long id, Integer maxDinnerSize, Integer location) throws BestKitchenException{
		
		try {
				
			if (id != null) {
				Table table = em.find(Table.class, id);
				table.setLocation(location);
				table.setMaxDinner(maxDinnerSize);
				em.merge(table);
			} else {
				Table table = new Table();
				table.setLocation(location);
				table.setLocation(maxDinnerSize);
				em.persist(table);
			}	
		} catch (Exception e) {
			throw new BestKitchenException("Error while creating table " + e.getMessage());
		}
	}
	
	public Long createBill(Long customerId, Long tableId, Long waiterId, Date billTimestamp) throws BestKitchenException{
		
		if (customerId == null) {
			throw new BestKitchenException("Customer is required");
		}
		
		if (tableId == null) {
			throw new BestKitchenException("Table is required");
		}
		
		if (waiterId == null) {
			throw new BestKitchenException("Waiter is required");
		}
		
		if (billTimestamp == null) {
			throw new BestKitchenException("Timestamp is required");
		}
		
		try {
			Customer customer = em.find(Customer.class, customerId);
			Table table = em.find(Table.class, tableId);
			Waiter waiter = em.find(Waiter.class, waiterId);
			
			Bill bill = new Bill(); 
			bill.setCustomer(customer);
			bill.setTable(table);
			bill.setWaiter(waiter);
			bill.setBillTimestamp(billTimestamp);
			em.persist(bill);
			
			return bill.getId();
	
		} catch (Exception e) {
			throw new BestKitchenException("Error while creating bill " + e.getMessage());
		}
	}
	
	public Long saveDetail(Long billId, Long chefId, Double value, String dinnerPlate) throws BestKitchenException{
		
		if (billId == null) {
			throw new BestKitchenException("Bill is required");
		}
		
		if (chefId == null) {
			throw new BestKitchenException("Chef is required");
		}
		
		if (value == null) {
			throw new BestKitchenException("Value is required");
		}
		
		if (dinnerPlate == null) {
			throw new BestKitchenException("Plate is required");
		}
		
		try {
			Bill bill = em.find(Bill.class, billId);
			Chef chef = em.find(Chef.class, chefId);
			
			BillDetail detail = new BillDetail();
			detail.setBill(bill);
			detail.setChef(chef);
			detail.setDinnerPlate(dinnerPlate);
			detail.setValue(value);
			em.persist(detail);
			
			return detail.getId();
			
		} catch (Exception e) {
			throw new BestKitchenException("Error while creating bill detail " + e.getMessage());
		}
	}
	
	public List<BestCustomers> findBestCustomers(Long maxValue) throws BestKitchenException{
		
		if (maxValue == null) {
			maxValue = 100000L;
		}
		
		StringBuffer customersSql = new StringBuffer(
				"SELECT c.id, c.name as name, c.last_name1 as ln1, SUM(bd.value) as value " + 
				"FROM customer c " + 
				"INNER JOIN bill b ON b.customer_fk=c.id " + 
				"INNER JOIN bill_detail bd ON bd.bill_fk=b.id " + 
				"GROUP BY c.id, c.name, c.last_name1 " + 
				"HAVING SUM(bd.value) > ?");
		
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		List<BestCustomers> bestCumstomersList = new ArrayList<BestCustomers>(); 
		
		try {
			conn = dataSource.getConnection();
			pst = conn.prepareStatement(customersSql.toString());
			pst.setLong(1, maxValue);
			pst.executeQuery();
			rs = pst.getResultSet();

			while (rs.next()) {
				BestCustomers bc = new BestCustomers();
				bc.setId(rs.getLong(1));
				bc.setName(rs.getString(2));
				bc.setLastName1(rs.getString(3));
				bc.setTotal(rs.getDouble(4));
				
				bestCumstomersList.add(bc);
			}
			
		} catch (SQLException e) {
			throw new BestKitchenException("Error while finding best users");
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
				if (conn != null)
					conn.close();
			} catch (SQLException ex) {
				throw new BestKitchenException("Error while finding best users");
			}
		}
		
		return bestCumstomersList;
	}
	
	
	public List<WaitersInfo> findWaitersTotalByMonth() throws BestKitchenException{

		StringBuffer waitersSql = new StringBuffer(
				"SELECT w.name as name, w.last_name1 as ln1, SUM(bd.value) as value, date_trunc('month', b.bill_timestamp) as month " + 
				"FROM waiter w " + 
				"LEFT OUTER JOIN bill b ON b.waiter_fk=w.id " + 
				"LEFT OUTER JOIN bill_detail bd ON bd.bill_fk=b.id " + 
				"GROUP BY w.id, w.name, w.last_name1 , 4 ");
		
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		List<WaitersInfo> bestCumstomersList = new ArrayList<WaitersInfo>(); 
		
		try {
			conn = dataSource.getConnection();
			pst = conn.prepareStatement(waitersSql.toString());
			pst.executeQuery();
			rs = pst.getResultSet();

			while (rs.next()) {
				WaitersInfo wi = new WaitersInfo();
				
				wi.setName(rs.getString(1));
				wi.setLastName1(rs.getString(2));
				wi.setValue(rs.getDouble(3));
				wi.setMonthDate(rs.getDate(4));
				
				bestCumstomersList.add(wi);
			}
			
		} catch (SQLException e) {
			throw new BestKitchenException("Error while finding waiters info");
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pst != null)
					pst.close();
				if (conn != null)
					conn.close();
			} catch (SQLException ex) {
				throw new BestKitchenException("Error while finding waiters info");
			}
		}
		
		return bestCumstomersList;
	}
	
}
