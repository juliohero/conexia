package com.conexia.view;

import java.util.List;

import com.conexia.business.ConexiaBusiness;
import com.conexia.dto.BestCustomers;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Theme("valo")
@SuppressWarnings("serial")
public class BestCustomersView extends VerticalLayout implements View{

	public BestCustomersView() {
		final VerticalLayout layout = new VerticalLayout();
		layout.addComponent(new Label("Waiters Info"));

		Table table = new Table("Waiters info");

		// Define two columns for the built-in container
		table.addContainerProperty("Name", String.class, null);
		table.addContainerProperty("Last Name",  String.class, null);
		table.addContainerProperty("Total",  Double.class, null);
		
		List<BestCustomers> customers = ConexiaBusiness.getInstance().findBestCustomers(null);
		if (customers != null && !customers.isEmpty()) {
			int id = 0;
			for (BestCustomers b : customers) {
				table.addItem(new Object[]{b.getName(), b.getLastName1(), b.getTotal()}, id);
				id ++;
			}
		}
		
        Button backBttn = new Button("Back to Main menu");
        backBttn.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
            	UI.getCurrent().getNavigator().navigateTo("");
            }
        });

        layout.addComponent(table);
        layout.addComponent(backBttn);
        
        
        addComponent(layout);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
	}

}
