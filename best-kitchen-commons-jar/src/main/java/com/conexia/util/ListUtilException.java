package com.conexia.util;

public class ListUtilException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public ListUtilException (String message){
		super(message);
	}
	
	public ListUtilException (Throwable cause){
		super(cause);
	}
	
	public ListUtilException (String message, Throwable cause){
		super(message, cause);
	}

}
