package com.conexia.assembler;

import java.util.List;

import com.conexia.dto.PersonVO;
import com.conexia.dto.VOAssembler;
import com.conexia.dto.VOAssemblerException;
import com.conexia.entity.Chef;

public class ChefVOAssembler extends VOAssembler<Chef, PersonVO> {

	private static final ChefVOAssembler instance = new ChefVOAssembler();
	 
    private ChefVOAssembler() {}
 
    public static ChefVOAssembler getInstance() {
        return instance;
    }

    @Override
    public void assembleVOToEntity(PersonVO vo, Chef entity) throws VOAssemblerException{
		super.assembleVOToEntity(vo, entity);
		
	}
	
    @Override
	public void assembleVOListToEntityList(List<PersonVO> voList, List<Chef> entityList) throws VOAssemblerException{
		if(voList.size() != entityList.size()) throw new VOAssemblerException("Different size list");
		for(int i=0; i<voList.size(); i++){
			this.assembleVOToEntity(voList.get(i), entityList.get(i));
		}
	}
	
    @Override
	public void assembleEntityToVO(Chef entity, PersonVO vo) throws VOAssemblerException{
		super.assembleEntityToVO(entity, vo);
	}
	
    @Override
	public void assembleEntityListToVOList(List<Chef> entityList, List<PersonVO> voList) throws VOAssemblerException{
		if(voList.size() != entityList.size()) throw new VOAssemblerException("Different size list");
		for(int i=0; i<entityList.size(); i++){
			this.assembleEntityToVO(entityList.get(i), voList.get(i));
		}
	}
    
}
