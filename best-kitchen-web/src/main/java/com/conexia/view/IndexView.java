package com.conexia.view;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Theme("valo")
@SuppressWarnings("serial")
public class IndexView extends VerticalLayout implements View{

	public IndexView() {
		final VerticalLayout layout = new VerticalLayout();

		layout.addComponent(new Label("Welcome To Conexia's Best Kitchen"));

        Button billBtn = new Button("Create Bill");
        billBtn.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
    			UI.getCurrent().getNavigator().navigateTo("bill_view");
            }
        });
        
        Button waitersBtn = new Button("Waiters Information");
        waitersBtn.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
            	UI.getCurrent().getNavigator().navigateTo("waiters_info_view");
            }
        });
        
        Button bestCusBtn = new Button("Best Customers");
        bestCusBtn.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
            	UI.getCurrent().getNavigator().navigateTo("best_customers_view");
            }
        });

        layout.addComponent(billBtn);
        layout.addComponent(waitersBtn);
        layout.addComponent(bestCusBtn);
        
        addComponent(layout);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
	}

}
