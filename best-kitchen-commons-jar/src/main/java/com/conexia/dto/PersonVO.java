package com.conexia.dto;

import java.io.Serializable;

public class PersonVO implements Serializable, CommonVO {
	private static final long serialVersionUID = 1L;

	private Long id;

	private String name;

	private String lastName1;
	
	private String lastName2;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName1() {
		return lastName1;
	}

	public void setLastName1(String lastName1) {
		this.lastName1 = lastName1;
	}

	public String getLastName2() {
		return lastName2;
	}

	public void setLastName2(String lastName2) {
		this.lastName2 = lastName2;
	}
}