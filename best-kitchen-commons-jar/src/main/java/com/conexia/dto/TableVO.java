package com.conexia.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;


public class TableVO implements Serializable, CommonVO{
	private static final long serialVersionUID = 1L;

	private Long id;

	private Integer maxDinner;

	private Integer location;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getMaxDinner() {
		return maxDinner;
	}

	public void setMaxDinner(Integer maxDinner) {
		this.maxDinner = maxDinner;
	}

	public Integer getLocation() {
		return location;
	}

	public void setLocation(Integer location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "Table [id=" + id + ", maxDinner=" + maxDinner + ", location=" + location + "]";
	}
}