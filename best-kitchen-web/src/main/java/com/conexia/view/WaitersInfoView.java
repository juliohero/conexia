package com.conexia.view;

import java.util.Date;
import java.util.List;

import com.conexia.business.ConexiaBusiness;
import com.conexia.dto.WaitersInfo;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Theme("valo")
@SuppressWarnings("serial")
public class WaitersInfoView extends VerticalLayout implements View{

	public WaitersInfoView() {
		final VerticalLayout layout = new VerticalLayout();
		layout.addComponent(new Label("Best Customers"));

		Table table = new Table("Best Customers");

		// Define two columns for the built-in container
		table.addContainerProperty("Name", String.class, null);
		table.addContainerProperty("Last Name",  String.class, null);
		table.addContainerProperty("Total",  Double.class, null);
		table.addContainerProperty("Date",  Date.class, null);
		
		List<WaitersInfo> waitersInfos = ConexiaBusiness.getInstance().findWaitersTotalByMonth();
		if (waitersInfos != null && !waitersInfos.isEmpty()) {
			int id = 0;
			for (WaitersInfo w : waitersInfos) {
				table.addItem(new Object[]{w.getName(), w.getLastName1(), w.getValue(), w.getMonthDate()}, id);
				id ++;
			}
		}
		
        Button backBttn = new Button("Back to Main menu");
        backBttn.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
            	UI.getCurrent().getNavigator().navigateTo("");
            }
        });

        layout.addComponent(table);
        layout.addComponent(backBttn);
        
        
        addComponent(layout);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
	}

}
