package com.conexia.dto;

import java.io.Serializable;

public class CustomerVO extends PersonVO implements Serializable, CommonVO {
	
	private static final long serialVersionUID = 1L;

	private String observation;

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}
}