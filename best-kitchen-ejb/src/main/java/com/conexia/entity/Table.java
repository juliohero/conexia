package com.conexia.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;


/**
 * The persistent class for the service database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name = "Table.findAll", query = "SELECT t FROM Table t")
})
@javax.persistence.Table(name="table_bill")
public class Table implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TABLE_ID_GENERATOR", sequenceName="TABLE_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TABLE_ID_GENERATOR")
	private Long id;

	@Column(name="max_diner_size")
	private Integer maxDinner;

	private Integer location;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getMaxDinner() {
		return maxDinner;
	}

	public void setMaxDinner(Integer maxDinner) {
		this.maxDinner = maxDinner;
	}

	public Integer getLocation() {
		return location;
	}

	public void setLocation(Integer location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "Table [id=" + id + ", maxDinner=" + maxDinner + ", location=" + location + "]";
	}
}