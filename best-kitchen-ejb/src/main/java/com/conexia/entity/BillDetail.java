package com.conexia.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * The persistent class for the service database table.
 * 
 */
@Entity
@NamedQueries({
})
@Table(name = "bill_detail")
public class BillDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BILL_DETAIL_ID_GENERATOR", sequenceName="BILL_DETAIL_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BILL_DETAIL_ID_GENERATOR")
	private Long id;

	@ManyToOne
	@JoinColumn(name="bill_fk")
	private Bill bill;
	
	@ManyToOne
	@JoinColumn(name="chef_fk")
	private Chef chef;
	
	private Double value;
	
	@Column(name="dinner_plate")
	private String dinnerPlate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public Chef getChef() {
		return chef;
	}

	public void setChef(Chef chef) {
		this.chef = chef;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public String getDinnerPlate() {
		return dinnerPlate;
	}

	public void setDinnerPlate(String dinnerPlate) {
		this.dinnerPlate = dinnerPlate;
	}
}