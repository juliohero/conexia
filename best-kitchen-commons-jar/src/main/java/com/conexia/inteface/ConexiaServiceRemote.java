package com.conexia.inteface;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import com.conexia.dto.BestCustomers;
import com.conexia.dto.CustomerVO;
import com.conexia.dto.PersonVO;
import com.conexia.dto.TableVO;
import com.conexia.dto.WaitersInfo;
import com.conexia.exception.BestKitchenException;

@Remote
public interface ConexiaServiceRemote {

	List<CustomerVO> findCustomers() throws BestKitchenException;
	List<TableVO> findTables() throws BestKitchenException;
	List<PersonVO> findWaiters() throws BestKitchenException;
	List<PersonVO> findChefs() throws BestKitchenException;
	void createOrUpdateCustomer(Long id, String firstName, String lastName1, String lastName2, String observation) throws BestKitchenException;
	void createOrUpdateChef(Long id, String firstName, String lastName1, String lastName2) throws BestKitchenException;
	void createOrUpdateTable(Long id, Integer maxDinnerSize, Integer location) throws BestKitchenException;
	void createOrUpdateWaiter(Long id, String firstName, String lastName1, String lastName2) throws BestKitchenException;
	Long createBill(Long customerId, Long tableId, Long waiterId, Date billTimestamp) throws BestKitchenException;
	Long saveDetail(Long billId, Long chefId, Double value, String dinnerPlate) throws BestKitchenException;
	List<BestCustomers> findBestCustomers(Long maxValue) throws BestKitchenException;
	List<WaitersInfo> findWaitersTotalByMonth() throws BestKitchenException;
	
}
