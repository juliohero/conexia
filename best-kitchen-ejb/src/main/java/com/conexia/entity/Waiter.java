package com.conexia.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;


/**
 * The persistent class for the service database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name = "Waiter.findAll", query = "SELECT w FROM Waiter w")
})
public class Waiter implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="WAITER_ID_GENERATOR", sequenceName="WAITER_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="WAITER_ID_GENERATOR")
	private Long id;

	private String name;

	@Column(name="last_name1")
	private String lastName1;
	
	@Column(name="last_name2")
	private String lastName2;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName1() {
		return lastName1;
	}

	public void setLastName1(String lastName1) {
		this.lastName1 = lastName1;
	}

	public String getLastName2() {
		return lastName2;
	}

	public void setLastName2(String lastName2) {
		this.lastName2 = lastName2;
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", name=" + name + ", lastName1=" + lastName1 + ", lastName2=" + lastName2 + "]";
	}
	
}