package com.conexia.assembler;

import java.util.List;

import com.conexia.dto.CustomerVO;
import com.conexia.dto.VOAssembler;
import com.conexia.dto.VOAssemblerException;
import com.conexia.entity.Customer;

public class CustomerVOAssembler extends VOAssembler<Customer, CustomerVO> {

	private static final CustomerVOAssembler instance = new CustomerVOAssembler();
	 
    private CustomerVOAssembler() {}
 
    public static CustomerVOAssembler getInstance() {
        return instance;
    }

    @Override
    public void assembleVOToEntity(CustomerVO vo, Customer entity) throws VOAssemblerException{
		super.assembleVOToEntity(vo, entity);
		
	}
	
    @Override
	public void assembleVOListToEntityList(List<CustomerVO> voList, List<Customer> entityList) throws VOAssemblerException{
		if(voList.size() != entityList.size()) throw new VOAssemblerException("Different size list");
		for(int i=0; i<voList.size(); i++){
			this.assembleVOToEntity(voList.get(i), entityList.get(i));
		}
	}
	
    @Override
	public void assembleEntityToVO(Customer entity, CustomerVO vo) throws VOAssemblerException{
		super.assembleEntityToVO(entity, vo);
	}
	
    @Override
	public void assembleEntityListToVOList(List<Customer> entityList, List<CustomerVO> voList) throws VOAssemblerException{
		if(voList.size() != entityList.size()) throw new VOAssemblerException("Different size list");
		for(int i=0; i<entityList.size(); i++){
			this.assembleEntityToVO(entityList.get(i), voList.get(i));
		}
	}
    
}
