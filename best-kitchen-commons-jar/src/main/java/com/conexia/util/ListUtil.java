package com.conexia.util;

import java.util.ArrayList;
import java.util.List;

public class ListUtil {

	public static <K> List<K> createDefaultInitializedList(Class<K> iniClass, int size) throws ListUtilException {
		List<K> list = new ArrayList<K>(size);
		for (int i=0; i<size; i++){
			try{
				list.add(iniClass.newInstance());
			}
			catch(Exception ex){
				throw new ListUtilException("Error creating item new instance for initialized list", ex);
			}
		}
		return list;
	}
	
	public static String toCommaSeparatedStringArray(Long[] array){
		StringBuilder stringArray = new StringBuilder();
		for(int i=0; i<array.length; i++){
			if(i>0){
				stringArray.append(",");
			}
			stringArray.append(array[i]);
		}
		return stringArray.toString();
	}
	
}