package com.conexia.view;

import java.util.Date;
import java.util.List;

import com.conexia.business.ConexiaBusiness;
import com.conexia.dto.CustomerVO;
import com.conexia.dto.PersonVO;
import com.conexia.dto.TableVO;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Theme("valo")
@SuppressWarnings("serial")
public class BillInfoView extends VerticalLayout implements View{
	
	public BillInfoView() {
		final VerticalLayout layout = new VerticalLayout();
		layout.addComponent(new Label("Register customer"));

		final Label lblCustomer = new Label("Customer");
		final ComboBox cmbCustomer = new ComboBox();
		
		final Label lblWaiter = new Label("Waiter");
		final ComboBox cmbWaiter = new ComboBox();
		
		final Label lblTable = new Label("Table");
		final ComboBox cmbTable = new ComboBox();
		
		final DateField dateField = new DateField();
		dateField.setValue(new Date());

		
		//Fill combos
		List<CustomerVO> customers = ConexiaBusiness.getInstance().findCustomers();
		if (customers != null && !customers.isEmpty()) {
			for (CustomerVO c: customers) {
				cmbCustomer.addItem(c.getId());
				cmbCustomer.setItemCaption(c.getId(), c.getName() + " " + c.getLastName1());
			}
		}
		
		List<PersonVO> waiters = ConexiaBusiness.getInstance().findWaiters();
		if (waiters != null && !waiters.isEmpty()) {
			for (PersonVO p: waiters) {
				cmbWaiter.addItem(p.getId());
				cmbWaiter.setItemCaption(p.getId(), p.getName() + " " + p.getLastName1());
			}
			
		}
		
		List<TableVO> tables = ConexiaBusiness.getInstance().findTables();
		if (tables != null && !tables.isEmpty()) {
			for (TableVO t: tables) {
				cmbTable.addItem(t.getId());
				cmbTable.setItemCaption(t.getId(), "Location: " + t.getLocation() + " - Max Dinner: " + t.getMaxDinner());
			}
			
		}
	
        Button saveBttn = new Button("Save Bill");
        saveBttn.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
            	try{
            		Long billId = ConexiaBusiness.getInstance().createBill((Long)cmbCustomer.getValue(), (Long)cmbTable.getValue(), (Long)cmbWaiter.getValue(), dateField.getValue());
            		VaadinSession.getCurrent().setAttribute("billId", billId);
            	} catch (Exception e) {
            		Notification.show(e.getMessage());
				}
            	
            	Notification.show("Saved!");
            }
        });
		
        Button backBttn = new Button("Back to Bill menu");
        backBttn.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
            	UI.getCurrent().getNavigator().navigateTo("bill_view");
            }
        });

        layout.addComponent(lblCustomer);
        layout.addComponent(cmbCustomer);
        layout.addComponent(lblTable);
        layout.addComponent(cmbTable);
        layout.addComponent(lblWaiter);
        layout.addComponent(cmbWaiter);
        layout.addComponent(dateField);
        layout.addComponent(saveBttn);
        layout.addComponent(backBttn);
        
        
        layout.addComponent(new Label(""));
		layout.addComponent(new Label("Register Detail"));

		final Label lblCooker = new Label("Chef");
		final ComboBox cmbChef = new ComboBox();
		
		final Label lblPlate = new Label("Plate");
		final TextField txtPlate = new TextField();
		
		final Label lblValue = new Label("Value");
		final TextField txtValue = new TextField();
		
		//Fill combos
		List<PersonVO> chefs = ConexiaBusiness.getInstance().findChefs();
		if (chefs != null && !chefs.isEmpty()) {
			for (PersonVO c: chefs) {
				cmbChef.addItem(c.getId());
				cmbChef.setItemCaption(c.getId(), c.getName() + " " + c.getLastName1());
			}
		}
		
        Button saveDetailBttn = new Button("Save Bill Detail");
        saveDetailBttn.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
            	try{
            		Long billId = (Long)VaadinSession.getCurrent().getAttribute("billId");
            		ConexiaBusiness.getInstance().saveDetail(billId, (Long)cmbChef.getValue(), 
            				Double.parseDouble(txtValue.getValue()), txtPlate.getValue());
            	} catch (Exception e) {
            		Notification.show(e.getMessage());
				}
            	
            	Notification.show("Saved!");
            }
        });
        
        layout.addComponent(lblCooker);
        layout.addComponent(cmbChef);
        layout.addComponent(lblPlate);
        layout.addComponent(txtPlate);
        layout.addComponent(lblValue);
        layout.addComponent(txtValue);
        layout.addComponent(saveDetailBttn);
        layout.addComponent(backBttn);
		
        addComponent(layout);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
	}

}
