package com.conexia.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.jboss.logging.Logger;

public class ServiceLocator {
	
	@SuppressWarnings("unused")
	private Map<String, java.sql.Connection> resourceCache;
	private Map<String, Object> ejbCache;
	private Map<String, Context> scopedContextCache;
	private static String DEFAULT_DISTINCT_NAME="";
	private static ServiceLocator instance = new ServiceLocator();
	
	private static Logger logger = Logger.getLogger(ServiceLocator.class);
	
	private ServiceLocator() {
		this.resourceCache = Collections.synchronizedMap(new HashMap<String, java.sql.Connection>());
		this.ejbCache = Collections.synchronizedMap(new HashMap<String, Object>());
		this.scopedContextCache = Collections.synchronizedMap(new HashMap<String, Context>());
	}
	
	public static ServiceLocator getInstance(){
		return instance;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T> T locateNonCachedRemoteEJBStateless(Class<T> viewType, String appName, String moduleName, String beanName) throws ServiceLocatorException {
		T ejb = null;
		try{
			Hashtable props = new Hashtable();
	        props.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
	        Context context = new javax.naming.InitialContext(props);
	        String jndi = "ejb:" + appName + "/" + moduleName + "/" + DEFAULT_DISTINCT_NAME + "/" + beanName + "!" + viewType.getName();
			ejb = (T)context.lookup(jndi);
		}
		catch(Exception ex){
			throw new ServiceLocatorException(ex.getMessage(), ex);
		}
		return ejb;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T locateNonCachedRemoteEJBStateless(Class<T> viewType, String appName, String moduleName, String beanName, 
			String host, Integer port, String username, String password) throws ServiceLocatorException {
		T ejb = null;
		try{
			final Properties ejbClientContextProps = new Properties();
			final String connectionName = host+port+username;
			
			ejbClientContextProps.put("endpoint.name", connectionName);
	        ejbClientContextProps.put("org.jboss.ejb.client.scoped.context", true);
	        ejbClientContextProps.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
	        ejbClientContextProps.put("remote.connectionprovider.create.options.org.xnio.Options.SSL_ENABLED", "false");
	        ejbClientContextProps.put("remote.connections", connectionName);
	        ejbClientContextProps.put("remote.connection." + connectionName + ".host", host);
	        ejbClientContextProps.put("remote.connection." + connectionName + ".port", port.toString());
	        ejbClientContextProps.put("remote.connection." + connectionName + ".username", username);
	        ejbClientContextProps.put("remote.connection." + connectionName + ".password", password);
	        
	        Context context = (Context) new InitialContext(ejbClientContextProps).lookup("ejb:");
	        String jndi = appName + "/" + moduleName + "/" + DEFAULT_DISTINCT_NAME + "/" + beanName + "!" + viewType.getName();
			ejb = (T)context.lookup(jndi);
		}
		catch(Exception ex){
			throw new ServiceLocatorException(ex.getMessage(), ex);
		}
		return ejb;
	}
 
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T> T locateRemoteEJBStateless(Class<T> viewType, String appName, String moduleName, String beanName) throws ServiceLocatorException {
		String key = viewType.getName()+"-"+appName+"-"+moduleName+"-"+beanName;
		//logger.debug("key: "+key);
		T ejb = null;
		if(this.ejbCache.containsKey(key)) {
			ejb = (T)this.ejbCache.get(key);
		}
		else{
			try{
				Hashtable props = new Hashtable();
		        props.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
		        Context context = new javax.naming.InitialContext(props);
		        String jndi = "ejb:" + appName + "/" + moduleName + "/" + DEFAULT_DISTINCT_NAME + "/" + beanName + "!" + viewType.getName();
				ejb = (T)context.lookup(jndi);
		        this.ejbCache.put(key, ejb);
			}
			catch(Exception ex){
				throw new ServiceLocatorException(ex.getMessage(), ex);
			}
		}
		return ejb;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T locateRemoteEJBStateless(Class<T> viewType, String appName, String moduleName, String beanName, String host, Integer port, String username, String password) throws ServiceLocatorException {
		String ejbCacheKey = viewType.getName()+"-"+appName+"-"+moduleName+"-"+beanName+"-"+host+"-"+port+"-"+username;
		String scopedContextCacheKey = host+"-"+port+"-"+username;
		//logger.debug("ejbCacheKey: "+ejbCacheKey);
		//logger.debug("scopedContextCacheKey: "+scopedContextCacheKey);
		T ejb = null;
		Context context = null;
		if(this.ejbCache.containsKey(ejbCacheKey)) {
			//logger.debug("get cached ejb");
			ejb = (T)this.ejbCache.get(ejbCacheKey);
		}
		else{
			if(this.scopedContextCache.containsKey(scopedContextCacheKey)){
				//logger.debug("get cached scoped context");
				context = this.scopedContextCache.get(scopedContextCacheKey);
			}
			else{
				try{
					logger.debug("lookup scoped context");
					//locating scoped context
					final Properties ejbClientContextProps = new Properties();
					final String connectionName = host+port+username;
					
					ejbClientContextProps.put("endpoint.name", connectionName);
			        ejbClientContextProps.put("org.jboss.ejb.client.scoped.context", true);
			        ejbClientContextProps.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
			        ejbClientContextProps.put("remote.connectionprovider.create.options.org.xnio.Options.SSL_ENABLED", "false");
			        ejbClientContextProps.put("remote.connections", connectionName);
			        ejbClientContextProps.put("remote.connection." + connectionName + ".host", host);
			        ejbClientContextProps.put("remote.connection." + connectionName + ".port", port.toString());
			        ejbClientContextProps.put("remote.connection." + connectionName + ".username", username);
			        ejbClientContextProps.put("remote.connection." + connectionName + ".password", password);
			        
			        context = (Context) new InitialContext(ejbClientContextProps).lookup("ejb:");
			        this.scopedContextCache.put(scopedContextCacheKey, context);
				}
				catch(Exception ex){
					throw new ServiceLocatorException("Error locating scoped context key "+scopedContextCacheKey, ex);
				}
			}
			//locating ejb
			try{
				//logger.debug("lookup ejb");
				String jndi = appName + "/" + moduleName + "/" + DEFAULT_DISTINCT_NAME + "/" + beanName + "!" + viewType.getName();
				ejb = (T)context.lookup(jndi);
		        this.ejbCache.put(ejbCacheKey, ejb);
			}
			catch(Exception ex){
				throw new ServiceLocatorException("Error locating  ejb key "+ejbCacheKey+" in scoped context key "+scopedContextCacheKey, ex);
			}
		}
		return ejb;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T locateRemoteEJBStateless(Class<T> viewType, String appName, String moduleName, String beanName, String host, Integer port) throws ServiceLocatorException {
		String ejbCacheKey = viewType.getName()+"-"+appName+"-"+moduleName+"-"+beanName+"-"+host+"-"+port;
		String scopedContextCacheKey = host+"-"+port;
		//logger.debug("ejbCacheKey: "+ejbCacheKey);
		//logger.debug("scopedContextCacheKey: "+scopedContextCacheKey);
		T ejb = null;
		Context context = null;
		if(this.ejbCache.containsKey(ejbCacheKey)) {
			//logger.debug("get cached ejb");
			ejb = (T)this.ejbCache.get(ejbCacheKey);
		}
		else{
			if(this.scopedContextCache.containsKey(scopedContextCacheKey)){
				//logger.debug("get cached scoped context");
				context = this.scopedContextCache.get(scopedContextCacheKey);
			}
			else{
				try{
					logger.debug("lookup scoped context");
					//locating scoped context
					final Properties ejbClientContextProps = new Properties();
					final String connectionName = host+port;
					
					ejbClientContextProps.put("endpoint.name", connectionName);
			        ejbClientContextProps.put("org.jboss.ejb.client.scoped.context", true);
			        ejbClientContextProps.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
			        ejbClientContextProps.put("remote.connectionprovider.create.options.org.xnio.Options.SSL_ENABLED", "false");
			        ejbClientContextProps.put("remote.connections", connectionName);
			        ejbClientContextProps.put("remote.connection." + connectionName + ".host", host);
			        ejbClientContextProps.put("remote.connection." + connectionName + ".port", port.toString());
			        
			        context = (Context) new InitialContext(ejbClientContextProps).lookup("ejb:");
			        this.scopedContextCache.put(scopedContextCacheKey, context);
				}
				catch(Exception ex){
					throw new ServiceLocatorException("Error locating scoped context key "+scopedContextCacheKey, ex);
				}
			}
			//locating ejb
			try{
				//logger.debug("lookup ejb");
				String jndi = appName + "/" + moduleName + "/" + DEFAULT_DISTINCT_NAME + "/" + beanName + "!" + viewType.getName();
				ejb = (T)context.lookup(jndi);
		        this.ejbCache.put(ejbCacheKey, ejb);
			}
			catch(Exception ex){
				throw new ServiceLocatorException("Error locating  ejb key "+ejbCacheKey+" in scoped context key "+scopedContextCacheKey, ex);
			}
		}
		return ejb;
	}
 
}