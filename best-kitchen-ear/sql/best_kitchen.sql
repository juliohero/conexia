--Tables
CREATE TABLE bill (
    id bigint NOT NULL,
    customer_fk bigint NOT NULL,
    table_fk bigint NOT NULL,
    waiter_fk bigint NOT NULL,
    bill_timestamp timestamp with time zone NOT NULL
);

CREATE TABLE bill_detail (
    id bigint NOT NULL,
    bill_id bigint NOT NULL,
    chef_id bigint NOT NULL,
    value double precision NOT NULL,
    dinner_plate character varying(128) NOT NULL
);

CREATE TABLE chef (
    id bigint NOT NULL,
    name character varying(64),
    last_name1 character varying(64),
    last_name2 character varying(64)
);

CREATE TABLE customer (
    id bigint NOT NULL,
    name character varying(64),
    last_name1 character varying(64),
    last_name2 character varying(64),
    observation character varying(128)
);

CREATE TABLE table_bill (
    id bigint NOT NULL,
    max_diner_size integer,
    location smallint
);

CREATE TABLE waiter (
    id bigint NOT NULL,
    name character varying(64),
    last_name1 character varying(64),
    last_name2 character varying(64)
);

--Sequences
CREATE SEQUENCE bill_detail_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE bill_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE chef_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE customer_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE table_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE waiter_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- Constraints
--Pkeys
ALTER TABLE ONLY bill_detail
    ADD CONSTRAINT bill_detail_pkey PRIMARY KEY (id);

ALTER TABLE ONLY bill
    ADD CONSTRAINT bill_pkey PRIMARY KEY (id);

ALTER TABLE ONLY chef
    ADD CONSTRAINT chef_pkey PRIMARY KEY (id);

ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (id);

ALTER TABLE ONLY "table"
    ADD CONSTRAINT table_pkey PRIMARY KEY (id);

ALTER TABLE ONLY waiter
    ADD CONSTRAINT waiter_pkey PRIMARY KEY (id);
    
--FKeys
ALTER TABLE ONLY bill_detail
    ADD CONSTRAINT bill_fkey FOREIGN KEY (bill_id) REFERENCES bill(id);

ALTER TABLE ONLY bill_detail
    ADD CONSTRAINT chef_fkey FOREIGN KEY (chef_id) REFERENCES chef(id);

ALTER TABLE ONLY bill
    ADD CONSTRAINT customer_fkey FOREIGN KEY (customer_fk) REFERENCES customer(id);

ALTER TABLE ONLY bill
    ADD CONSTRAINT table_fkey FOREIGN KEY (table_fk) REFERENCES table_bill(id);

ALTER TABLE ONLY bill
    ADD CONSTRAINT waiter_fkey FOREIGN KEY (waiter_fk) REFERENCES waiter(id);